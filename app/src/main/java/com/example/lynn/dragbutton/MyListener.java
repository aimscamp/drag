package com.example.lynn.dragbutton;

import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import static com.example.lynn.dragbutton.MainActivity.*;

/**
 * Created by lynn on 6/8/2016.
 */
public class MyListener implements View.OnTouchListener {
    private int offSetX;
    private int offSetY;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)v.getLayoutParams();

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            offSetX = (int)(event.getRawX() - layoutParams.leftMargin);
            offSetY = (int)(event.getRawY() - layoutParams.topMargin);
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            layoutParams.leftMargin = (int) (event.getRawX() - offSetX);
            layoutParams.topMargin = (int) (event.getRawY() - offSetY);

            v.setLayoutParams(layoutParams);
        }

        return true;
    }

}
