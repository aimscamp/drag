package com.example.lynn.dragbutton;

import android.content.Context;
import android.widget.Button;
import android.widget.RelativeLayout;

import static com.example.lynn.dragbutton.MainActivity.*;

/**
 * Created by lynn on 6/8/2016.
 */
public class MyView extends RelativeLayout {

    public MyView(Context context) {
        super(context);

        buttons = new Button[word.length()];

        for (int counter=0;counter<buttons.length;counter++) {
            buttons[counter] = new Button(context);

            buttons[counter].setText(word.substring(counter,counter+1));

            buttons[counter].setOnTouchListener(listener);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(200,200);

            buttons[counter].setLayoutParams(layoutParams);

            addView(buttons[counter]);
        }
    }

}
